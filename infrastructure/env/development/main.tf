#call modules, locals and data-sources to create all resources

#terraform {
# Leave the config for this backend unspecified so Terraform can fill it in. Uncomment if using S3 backend
#  backend "s3" {}
#  required_version = ">= 0.12"
#}
variable "do_token" {}
#variable "FP" {}

provider "digitalocean" {
  token = var.do_token
}


#Create VPC
resource "digitalocean_vpc" "pledge-vpc" {
  name     = "pledge-vpc"
  region   = var.region[0]
  ip_range = "10.131.0.0/24"
}



#Create the 2 Droplets
resource "digitalocean_droplet" "droplets" {
  for_each = toset(var.droplet_names)
  # Obtain your ssh_key id number via your account.
  ssh_keys           = [25260392]    # Key id retrieved through the DO API, See https://developers.digitalocean.com/documentation/v2/#list-all-keys
  image              = var.os[0]
  region             = var.region[0]
  size               = "s-1vcpu-2gb"
  private_networking = true
  backups            = true
  name               = "${each.key}"
  vpc_uuid = digitalocean_vpc.pledge-vpc.id
}



#Create a project and link the resources to the project
resource "digitalocean_project" "pledge-project" {
  name        = "pledge-project"
  description = "An Infrastructure project"
  purpose     = "Test"
  environment = "Development"
  resources   = [digitalocean_droplet.droplets["pledge-app"].urn, digitalocean_droplet.droplets["pledge-db"].urn]
}



#Save the IPs and names in a file for later use with Ansible
#Export details to Ansible directory
resource "local_file" "pledge-app" {
  filename = "../../../application/inventories/development/host_vars/pledge-app.yml"
  content = join("\n", concat([join(": ", ["pledge_app_public"], "${digitalocean_droplet.droplets["pledge-app"].*.ipv4_address}")], [join(": ", ["pledge_app_private"], "${digitalocean_droplet.droplets["pledge-app"].*.ipv4_address_private}")]))
}


resource "local_file" "pledge-db" {
  filename = "../../../application/inventories/development/host_vars/pledge-db.yml"
  content = join("\n", concat([join(": ", ["pledge_db_public"], "${digitalocean_droplet.droplets["pledge-db"].*.ipv4_address}")], [join(": ", ["pledge_db_private"], "${digitalocean_droplet.droplets["pledge-db"].*.ipv4_address_private}")]))
}


resource "local_file" "pledge-vpc" {
  filename = "../../../application/inventories/development/group_vars/all.yml"
  content = join(": ", ["pledge_vpc_cidr"], ["${digitalocean_vpc.pledge-vpc.ip_range}"])
}



