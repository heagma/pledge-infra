#contains declarations of variables used in main.tf
# Default Os


variable "os" {
  description = "Default LTS"
  type = list(string)
  default     = ["ubuntu-18-04-x64"]
}


#Default datacenter
variable "region" {
  description = "Digital Ocean Singapore Data Center 1"
  type = list(string)
  default     = ["sgp1"]
}



variable "droplet_names" {
  description = "Tag names for the droplets"
  type = list(string)
  default = ["pledge-app", "pledge-db"]
}





 