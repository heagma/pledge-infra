#contains outputs from the resources created in main.tf
#Output Droplet details
output "name_pledge_app" {
  value = digitalocean_droplet.droplets["pledge-app"].name
}

output "public_ip_pledge_app" {
  value = digitalocean_droplet.droplets["pledge-app"].ipv4_address
}

output "private_ip_pledge_app" {
  value = digitalocean_droplet.droplets["pledge-app"].ipv4_address_private
}


#Output DB details
output "name_pledge_db" {
  value = digitalocean_droplet.droplets["pledge-db"].name
}

output "public_ip_pledge_db" {
  value = digitalocean_droplet.droplets["pledge-db"].ipv4_address
}

output "private_ip_pledge_db" {
  value = digitalocean_droplet.droplets["pledge-db"].ipv4_address_private
}



#Output VPC
output "vpc_ip_range" {
  value = digitalocean_vpc.pledge-vpc.ip_range
}