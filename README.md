# PLEDGE

An infrastructure project which uses Terraform to spin up resources in Digital Ocean as follow:
- 1 VPC
- 2 Droplets

And configure through Ansible the resources to run a Gin Gonic API which connects to a Postgresql database. 

Application and Database are in separated droplets. The Application connect to the DB through its private IP in the VPC network, so the DB is not accesible from public IPs but only on port 22 with SSH.
 


## Set Up

Clone/Download this repository.

Install and set up Terraform, see [here](https://learn.hashicorp.com/terraform/getting-started/install.html).

Get an API Token in the Digital Ocean dashboard and then save it in a secure location. 


```export DO_TOKEN="here-your-api-token"```


Sometimes it is possible you will need the fingerprint of your local keys.If so, get it with:

```ssh-keygen -E md5 -lf ~/.ssh/id_rsa.pub | awk '{print $2}'```

Export the fingerprint without the MD5:

```export FP=0E:1X:2A:3M:4P:5L:6E```


### Note:
You will need to edit the `ssh_key` attribute on the Terraform configuration so the Droplets can be created with a key-pair that you already have stored in Digital Ocean. To get your stored keys see [here](https://developers.digitalocean.com/documentation/v2/#ssh-keys).




## Spinning Droplets Up

**Initialize Terraform:**

```terraform init --options [DIR]```



If want to use a remote backend, uncomment the lines on the Terraform configuration and set it up. tfstates should not be kept locally.

`terraform plan -var="do_token=$DO_TOKEN"`

`terraform apply -var="do_token=$DO_TOKEN"`



**Configuration with Ansible:**

- Install and set up Ansible in your control node, see [here](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html).


- Wait until all the resources are created by Terraform as Ansible will reject to connect. 

- Execute the **initial-setup** playbook as root individually for each host (1 of them will be unreachable if executed for all). Before executing it make sure to edit the var file in the var folder of the playbook for the name of the user:

`ansible-playbook initial-setup.yml -i inventories/development/ --limit [host] -u root`

For security, the playbook disable the SSH access for the root user.

- Run **db-setup** playbook:

`ansible-playbook db-setup.yml -i inventories/development/`


- Finally run **app-deploy** to place the code in the webserver, set up env variables, build, test and start the application:

`ansible-playbook app-deploy.yml -i inventories/development/`



- Now you can go to the web browser and point it to your droplet app public IP. Nginx will proxy it to https:[]:8080


## For Backups and Logging:
Is possible to create a volume attached to the DB Droplet and back the databases with the Postgresql pg_dump utility.

Exporting the necessary **logs** with a cron job and Rsyslog is a way of implementing a simply log keeper. Logs can be then be analyzed and exported to an external server. There are different other solutions for logging aggregation and analysis. Elastic, Fluentd, Prometheus and Graylog to name a few.


## Improvements:
- Build, Test and Run process can be separated from the Deploy playbook if needed.
- Create more custom Firewalls on resource creation with Terraform to the Digital Ocean resources to Allow/Deny connections.
- Allow only access to DB server through SSH from a bastion host or a single IP/Net, for instance, creating a VPN and connecting from there only. 
- Make the Infrastructure Immutable by design.
- Credentials should not be kept in local files, and although exporting them as env variables is a better approach, it should be considered to use Hashicorp Vault + Raft + keybase or another one like CICD tools (Jenkins, Gitlab CI) to store credentials. Ansible Vault probably not enough.
- This approach is not 100% immutable IAC , this is achievable with a combination of Ansible + Packer to create custom AMIs with everything on it and Terraform to deploy. And any changes that needs to be done, then the VM is destroyed and recreated with a new one. This can be accomplished with AWS Autoscaling groups to avoid downtime.
